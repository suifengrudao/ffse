# (C) 2018 University of Bristol. See License.txt


include CONFIG

TOOLS = $(patsubst %.cpp,%.o,$(wildcard Tools/*.cpp))

CLIENT = $(patsubst %.cpp,%.o,$(wildcard ffse_Client/*.cpp))

SERVER = $(patsubst %.cpp,%.o,$(wildcard ffse_Server/*.cpp))

NETWORKING = $(patsubst %.cpp,%.o,$(wildcard Networking/*.cpp))

Exceptions = $(patsubst %.cpp,%.o,$(wildcard Exceptions/*.cpp))

COMPLETE = $(TOOLS) $(SERVER) $(CLIENT) $(Exceptions) $(NETWORKING)

# used for dependency generation
OBJS = $(COMPLETE)
DEPS := $(OBJS:.o=.d)

-include $(DEPS)

%.o: %.cpp
	$(CXX) $(CFLAGS) -MMD -c -o $@ $<


# make order
clean:
	-rm */*.o *.o */*.d *.d *.x core.* *.a gmon.out

clean-local:
	-rm -r Client_Storage/*.sav Server_Storage/*.dat

online: client.x server.x

client.x: client.cpp $(COMPLETE) 
	$(CXX) $(CFLAGS) client.cpp -o client.x $(COMPLETE) $(LDLIBS)

server.x: server.cpp $(COMPLETE) 
	$(CXX) $(CFLAGS) server.cpp -o server.x $(COMPLETE) $(LDLIBS)




