// (C) 2019 University of NKU. Free for used
// Author: stoneboat@mail.nankai.edu.cn

/*
* client_core.cpp
*
*/

#include "client_core.h"
#include "Tools/ezOptionParser.h"
#include "Exceptions/Exceptions.h"
#include "Networking/ServerSocket.h"
#include "Networking/sockets.h"
#include "Networking/data.h"
#include "Tools/time-func.h"

#include <sse/crypto/random.hpp>
#include <sse/crypto/hmac.hpp>

#include <iostream>
#include <cstdint>
#include <math.h> 
#include <sys/time.h>

using namespace std;

typedef struct load_kw_indices_struct{
   client_core* obj;
   std::string folder_name;
   int t_id;
} kw_indices_info;

typedef struct thread_search_struct{
   client_core* obj;
   int t_id;
   double cp_time;
   double commu_time;
} thread_search_info;

client_core::client_core(int argc, const char** argv){
	ez::ezOptionParser opt;

    opt.syntax = "./client.x [OPTIONS]\n";
    opt.add(
        "", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "Directory containing the client data (default: Client_Storage)", // Help description.
        "-d", // Flag token.
        "--dir" // Flag token.
    );
    opt.add(
        "1", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "number of the threads(default 1)", // Help description.
        "-nt", // Flag token.
        "--number_threads" // Flag token.
    );
    opt.add(
        "localhost", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "Host where Server.x is running.", // Help description.
        "-h", // Flag token.
        "--hostname" // Flag token.
    );
    opt.add(
      "5000", // Default.
      0, // Required?
      1, // Number of args expected.
      0, // Delimiter if expecting multiple args.
      "Port number base to attempt to start connections from (default: 5000)", // Help description.
      "-pn", // Flag token.
      "--portnumbase" // Flag token.
    );
    opt.add(
      "1", // Default.
      0, // Required?
      1, // Number of args expected.
      0, // Delimiter if expecting multiple args.
      "scale of the test datasets (default: 1)", // Help description.
      "-x", // Flag token.
      "--scale" // Flag token.
    );
    opt.add(
        "", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "benchmark the update operation", // Help description.
        "-u", // Flag token.
        "--update" // Flag token.
    );
    opt.add(
        "", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "benchmark the search operation", // Help description.
        "-s", // Flag token.
        "--search" // Flag token.
    );
    opt.add(
        "", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "load keywords indices", // Help description.
        "-l", // Flag token.
        "--load_kw_indices" // Flag token.
    );


    opt.parse(argc, argv);

    opt.get("--number_threads")->getInt(nthreads);
    opt.get("--hostname")->getString(hostname);
    opt.get("--portnumbase")->getInt(pnbase);
    opt.get("--scale")->getInt(scale);



    if (opt.isSet("--dir"))
    {
        opt.get("--dir")->getString(PREP_DATA_PREFIX);
        PREP_DATA_PREFIX += "/";
    }
    else{
        PREP_DATA_PREFIX = "Client_Storage/";
    }

    if (opt.isSet("--update"))
    {
        test_update = true;
    }
    else{
        test_update = false;
    }

    if (opt.isSet("--search"))
    {
        test_search = true;
    }
    else{
        test_search = false;
    }

    if (opt.isSet("--load_kw_indices"))
    {
        load_kw_indices = true;
    }
    else{
        load_kw_indices = false;
    }

    /*
    *	create the local data folder if it is not exists
    */
    if(!is_directory(PREP_DATA_PREFIX)){
    	if(!create_directory(PREP_DATA_PREFIX,S_IRWXU)){
    		throw file_error("could not mkdir " + PREP_DATA_PREFIX);
    	}
    }

    /*
    *	load the client local master key, otherwise generate one first
    */
    std::string client_master_key_path = PREP_DATA_PREFIX + "derivation_master.key";
    ifstream client_master_key_in(client_master_key_path.c_str());
    if (client_master_key_in.good() != true){
    	// generate new master key first
    	keyed_prf = new sse::crypto::Prf<ffse::kKey_Size>();
    	// then store it back
    	ofstream client_master_key_out(client_master_key_path.c_str());
    	client_master_key_out << master_derivation_key();
    	client_master_key_out.close();
    }else{
    	// load the master key
    	stringstream client_master_key_buf;
    	client_master_key_buf << client_master_key_in.rdbuf();
    	keyed_prf = new sse::crypto::Prf<ffse::kKey_Size>(client_master_key_buf.str());
    }

    /*
    *   load the token map file map wid to (id,key) pairs, otherwise generate one first
    */
    token_map.resize(nthreads);
    for(int i=0; i<nthreads; i++){
        std::string token_map_path = PREP_DATA_PREFIX + "token_map"+ to_string(i) +".sav";
        if(!is_directory(token_map_path)){
            token_map[i] = new ssdmap::bucket_map<uint32_t, std::array<uint8_t, ffse::kMap_Size>>(token_map_path,1000000);
        }else{
            token_map[i] = new ssdmap::bucket_map<uint32_t, std::array<uint8_t, ffse::kMap_Size>>(token_map_path);
        }
    }
    
    

    //token_map
    /*
    *	load the client local map file which store the pair(word, word_id) if it exists
    */
    indices_map.resize(nthreads);
    for(int i=0; i<nthreads; i++){
        string client_indice_file_path = PREP_DATA_PREFIX + "kw_indice"+ to_string(i) +".csv";
        ifstream keyword_indices_in(client_indice_file_path);
        if(keyword_indices_in.good())
        {
            if(!parse_keyword_map(keyword_indices_in, indices_map[i])){
                throw file_error("could not load keyword indices_map\t"+client_indice_file_path);
            }
        }
        cout<<"load kw_indice" + to_string(i) +".csv complete\n";

        keyword_indices_in.close();
    }

    // keyword_indexer_stream_.open(client_indice_file_path, std::ios_base::app | std::ios_base::out);
    // if (!keyword_indexer_stream_.is_open()) {
    //     keyword_indexer_stream_.close();
    //     throw file_error("Could not open keyword indices_map " + client_indice_file_path);
    // }
    // keyword_indexer_stream_.close();

    /*
    *   initialization networking part
    */
    int port_num = pnbase;
    int inst = -1;
    socket_num.resize(nthreads);
    for (int i=0; i<nthreads; i++)
    {
        inst = -1;
        //set_up_client_socket(socket_num[i], hn[i].c_str(), pn+i);
        set_up_client_socket(socket_num[i], hostname.c_str(), port_num);
        send(socket_num[i], (octet*)&i, sizeof(i));
        cout << "Sent " << i << " to " << hostname.c_str() << ":" << port_num << endl;
        port_num += ffse::port_increase;
        while (inst != GO) 
        { 
          receive(socket_num[i], inst); 
        }
        cout << "Connected to server "<<i<< endl;
        inst = -1;
    }


    /*
    *   initialize the transfer buffer
    */
    transfer_update_buffer.resize(nthreads);
    transfer_update_ptr.resize(nthreads);
    transfer_search_buffer.resize(nthreads);
    transfer_search_ptr.resize(nthreads);
    for(int i=0; i<nthreads; i++){
        transfer_update_buffer[i] = new uint8_t[ffse::kBlock_Size*ffse::max_TransferBlocks];
        transfer_search_buffer[i] = new uint8_t[ffse::kMap_Size*ffse::max_TransferBlocks];
        transfer_update_ptr[i] = 0;
        transfer_search_ptr[i] = 0;
    }

    /*
    *   initialize the id counter
    */
    id_counter.resize(nthreads);
    for(int i=0; i<nthreads; i++){
        id_counter[i] = i*ffse::id_counter_offset;
    }

    // for multi-thread
    mutex_go = PTHREAD_MUTEX_INITIALIZER;

    // record of the communication bytes
    sent_bytes.resize(nthreads);
    for(int i=0; i<nthreads; i++){
        sent_bytes[i] = 0;
    }
}

client_core::~client_core(){
    delete keyed_prf;
    for(int i=0; i<nthreads; i++){
        close(socket_num[i]);
        delete transfer_update_buffer[i];
        delete transfer_search_buffer[i];
        delete token_map[i];
    }
}

void client_core::update(const uint32_t& wid, uint32_t fid, const bool& op, int thread_num){
    uint8_t block[ffse::kBlock_Size];

    // generate new key
    uint8_t r[ffse::kKey_Size];
    sse::crypto::random_bytes(ffse::kKey_Size, r);
    array<uint8_t, ffse::kKey_Size> key_star = keyed_prf->prf(r, ffse::kKey_Size);

    // generate new id, directly store it in the first 8 bytes in the block
    sse::crypto::random_bytes(ffse::kID_Size, block);

    //generate the mask
    sse::crypto::Prf<ffse::kValue_Size> tmp_keyed_prf((void*)key_star.data(),ffse::kKey_Size);
    array<uint8_t, ffse::kValue_Size> mask = tmp_keyed_prf.prf(block, ffse::kID_Size);

    // append the operation after the file ind
    fid = (fid<<1);
    fid |= op;

    /*
    *    mask the value part
    */
    array<uint8_t, ffse::kMap_Size> pre_bucket{{0x00}};
    bool found = token_map[thread_num]->get(wid,pre_bucket);

    uint8_t* p = (uint8_t *)& fid;
    

    for(size_t i=0; i< ffse::kMap_Size; i++){
        mask[i] ^= pre_bucket[i];
    }

    int offset = ffse::kMap_Size;
    for(size_t i=0; i<ffse::Ind_Size; i++){
        mask[offset++] ^= p[i];
    }

    /*
    *    generate the new block, remember the id* has been placed ahead
    */
    memcpy(block+ffse::kID_Size, mask.data(),ffse::kValue_Size);


    /*
    *  update the token map
    */
    memcpy(pre_bucket.data(),block,ffse::kID_Size);
    memcpy(pre_bucket.data()+ffse::kID_Size,key_star.data(),ffse::kKey_Size);
    if(found){
        token_map[thread_num]->at(wid) = pre_bucket;
    }else{
        token_map[thread_num]->add(wid,pre_bucket);
    }

    send(socket_num[thread_num], UPDATE);
    send_to(thread_num, block, ffse::kBlock_Size);
}

void client_core::adv_update_list(const uint32_t& wid, std::vector<uint32_t>& fids, int thread_num){
    if(fids.empty()){
        return;
    }

    using ffse::CT;

    int nblocks = ceil(fids.size()/(double (CT)));
    for(int i=fids.size(); i<nblocks*CT; i++){
        fids.push_back(0);//padded the dummy fids
    }

    int bucket = 1;
    uint8_t* block_ptr;
    const size_t CT_Value_Size = ffse::kValue_Size + (CT-1)*ffse::Ind_Size;
    const size_t CT_Block_Size = ffse::kBlock_Size + (CT-1)*ffse::Ind_Size;
    const int CT_max_TransferBlocks = (ffse::max_TransferBlocks*ffse::kBlock_Size)/CT_Block_Size;

    if(CT_Block_Size*nblocks+transfer_update_ptr[thread_num] > ffse::kBlock_Size*ffse::max_TransferBlocks){
        //cout<<"nblocks:\t "<<nblocks<<"\tbuffer len\t" << (transfer_update_ptr[thread_num]/ffse::kBlock_Size)<<endl;
        printf("..");
        flush_update_send_to(thread_num);
        bucket = ceil(double(nblocks)/CT_max_TransferBlocks);
    }

    // tmp variable
    array<uint8_t, ffse::kKey_Size> key_star;
    array<uint8_t, CT_Value_Size> mask;
    array<uint8_t, ffse::kMap_Size> pre_bucket{{0x00}};
    bool found;
    uint8_t* p;
    int offset;
    int fid_offset = 0;
    size_t block_id;

    // generate needed random. For each 32 bytes used for new key
    uint8_t* r = new uint8_t[ffse::kKey_Size*nblocks];
    uint8_t* ptr = r;
    sse::crypto::random_bytes(ffse::kKey_Size*nblocks, r);

    // get the W[wid].id and W[wid].key from local map
    found = token_map[thread_num]->get(wid,pre_bucket);

    // generate the new blocks
    for(int m=0; m< bucket-1; m++){
        block_ptr = transfer_update_buffer[thread_num] + transfer_update_ptr[thread_num];
        for(int n=0; n<CT_max_TransferBlocks; n++){
            key_star = keyed_prf->prf(ptr, ffse::kKey_Size);
            ptr += ffse::kKey_Size;
            sse::crypto::Prf<CT_Value_Size> tmp_keyed_prf((void*)key_star.data(),ffse::kKey_Size);
            block_id = id_counter[thread_num];
            id_counter[thread_num]++;
            mask = tmp_keyed_prf.prf((uint8_t *)& block_id, ffse::kID_Size);

            for(int k=0; k<CT; k++){
                fids[fid_offset] = (fids[fid_offset] << 1);
                fids[fid_offset] |= 1; // add one
                p = (uint8_t *)& fids[fid_offset];
                fid_offset++;
                // mask the ind part
                offset = ffse::kMap_Size + k*ffse::Ind_Size;
                for(size_t i=0; i<ffse::Ind_Size; i++){
                    mask[offset++] ^= p[i];
                }
            }

            // mask the key part
            for(size_t i=0; i< ffse::kMap_Size; i++){
                mask[i] ^= pre_bucket[i];
            }


            // update the new token map
            memcpy(pre_bucket.data(), (uint8_t *)& block_id,ffse::kID_Size);
            memcpy(pre_bucket.data()+ffse::kID_Size, key_star.data(),ffse::kKey_Size);

            // generate the new block
            memcpy(block_ptr, (uint8_t *)& block_id,ffse::kID_Size);
            block_ptr += ffse::kID_Size;
            memcpy(block_ptr, mask.data(),CT_Value_Size);
            block_ptr += CT_Value_Size;
        }
        transfer_update_ptr[thread_num] = block_ptr - transfer_update_buffer[thread_num];
        flush_update_send_to(thread_num);
    }

    block_ptr = transfer_update_buffer[thread_num] + transfer_update_ptr[thread_num];

    for(int n=0; n < nblocks-(bucket-1)*ffse::max_TransferBlocks; n++){
        key_star = keyed_prf->prf(ptr, ffse::kKey_Size);
        ptr += ffse::kKey_Size;
        sse::crypto::Prf<CT_Value_Size> tmp_keyed_prf((void*)key_star.data(),ffse::kKey_Size);
        block_id = id_counter[thread_num];
        id_counter[thread_num]++;
        mask = tmp_keyed_prf.prf((uint8_t *)& block_id, ffse::kID_Size);

        for(int k=0; k<CT; k++){
            fids[fid_offset] = (fids[fid_offset] << 1);
            fids[fid_offset] |= 1; // add one
            p = (uint8_t *)& fids[fid_offset];
            fid_offset++;
            // mask the ind part
            offset = ffse::kMap_Size + k*ffse::Ind_Size;
            for(size_t i=0; i<ffse::Ind_Size; i++){
                mask[offset++] ^= p[i];
            }
        }

        // mask the key part
        for(size_t i=0; i< ffse::kMap_Size; i++){
            mask[i] ^= pre_bucket[i];
        }


        // update the new token map
        memcpy(pre_bucket.data(), (uint8_t *)& block_id,ffse::kID_Size);
        memcpy(pre_bucket.data()+ffse::kID_Size, key_star.data(),ffse::kKey_Size);

        // generate the new block
        memcpy(block_ptr, (uint8_t *)& block_id,ffse::kID_Size);
        block_ptr += ffse::kID_Size;
        memcpy(block_ptr, mask.data(),CT_Value_Size);
        block_ptr += CT_Value_Size;
    }

    transfer_update_ptr[thread_num] = block_ptr - transfer_update_buffer[thread_num];
    // write back to the token map
    if(found){
        token_map[thread_num]->at(wid) = pre_bucket;
    }else{
        token_map[thread_num]->add(wid,pre_bucket);
    }

    // send(socket_num[thread_num], UPDATE);
    // send_to(thread_num, block, ffse::kBlock_Size*nblocks);

    delete r;
}
void client_core::add_inverted_list(const uint32_t& wid, vector<uint32_t>& fids, int thread_num){
    if(fids.empty()){
        return;
    }
    int nblocks = fids.size();
    int bucket = 1;
    uint8_t* block_ptr;

    if(ffse::kBlock_Size*nblocks+transfer_update_ptr[thread_num] > ffse::kBlock_Size*ffse::max_TransferBlocks){
        //cout<<"nblocks:\t "<<nblocks<<"\tbuffer len\t" << (transfer_update_ptr[thread_num]/ffse::kBlock_Size)<<endl;
        printf("..");
        flush_update_send_to(thread_num);
        bucket = ceil(double(nblocks)/ffse::max_TransferBlocks);
    }

    // tmp variable
    array<uint8_t, ffse::kKey_Size> key_star;
    array<uint8_t, ffse::kValue_Size> mask;
    array<uint8_t, ffse::kMap_Size> pre_bucket{{0x00}};
    bool found;
    uint8_t* p;
    int offset;
    size_t block_id;

    // generate needed random. For each 32 bytes used for new key
    uint8_t* r = new uint8_t[ffse::kKey_Size*nblocks];
    uint8_t* ptr = r;
    sse::crypto::random_bytes(ffse::kKey_Size*nblocks, r);

    // get the W[wid].id and W[wid].key from local map
    found = token_map[thread_num]->get(wid,pre_bucket);

    // generate the new blocks
    for(int m=0; m< bucket-1; m++){
        block_ptr = transfer_update_buffer[thread_num] + transfer_update_ptr[thread_num];
        for(int n=0; n<ffse::max_TransferBlocks; n++){
            key_star = keyed_prf->prf(ptr, ffse::kKey_Size);
            ptr += ffse::kKey_Size;
            sse::crypto::Prf<ffse::kValue_Size> tmp_keyed_prf((void*)key_star.data(),ffse::kKey_Size);
            block_id = id_counter[thread_num];
            id_counter[thread_num]++;
            mask = tmp_keyed_prf.prf((uint8_t *)& block_id, ffse::kID_Size);

            offset = m*ffse::max_TransferBlocks + n;
            fids[offset] = (fids[offset] << 1);
            fids[offset] |= 1; // add one
            p = (uint8_t *)& fids[offset];

            // mask the data
            for(size_t i=0; i< ffse::kMap_Size; i++){
                mask[i] ^= pre_bucket[i];
            }
            offset = ffse::kMap_Size;
            for(size_t i=0; i<ffse::Ind_Size; i++){
                mask[offset++] ^= p[i];
            }

            // update the new token map
            memcpy(pre_bucket.data(), (uint8_t *)& block_id,ffse::kID_Size);
            memcpy(pre_bucket.data()+ffse::kID_Size, key_star.data(),ffse::kKey_Size);

            // generate the new block
            memcpy(block_ptr, (uint8_t *)& block_id,ffse::kID_Size);
            block_ptr += ffse::kID_Size;
            memcpy(block_ptr, mask.data(),ffse::kValue_Size);
            block_ptr += ffse::kValue_Size;
        }
        transfer_update_ptr[thread_num] = block_ptr - transfer_update_buffer[thread_num];
        flush_update_send_to(thread_num);
    }

    block_ptr = transfer_update_buffer[thread_num] + transfer_update_ptr[thread_num];

    for(int n=0; n < nblocks-(bucket-1)*ffse::max_TransferBlocks; n++){
        key_star = keyed_prf->prf(ptr, ffse::kKey_Size);
        ptr += ffse::kKey_Size;
        sse::crypto::Prf<ffse::kValue_Size> tmp_keyed_prf((void*)key_star.data(),ffse::kKey_Size);
        block_id = id_counter[thread_num];
        id_counter[thread_num]++;
        mask = tmp_keyed_prf.prf((uint8_t *)& block_id, ffse::kID_Size);

        offset = (bucket-1)*ffse::max_TransferBlocks + n;
        fids[offset] = (fids[offset] << 1);
        fids[offset] |= 1; // add one
        p = (uint8_t *)& fids[offset];

        // mask the data
        for(size_t i=0; i< ffse::kMap_Size; i++){
            mask[i] ^= pre_bucket[i];
        }
        offset = ffse::kMap_Size;
        for(size_t i=0; i<ffse::Ind_Size; i++){
            mask[offset++] ^= p[i];
        }

        // update the new token map
        memcpy(pre_bucket.data(), (uint8_t *)& block_id,ffse::kID_Size);
        memcpy(pre_bucket.data()+ffse::kID_Size, key_star.data(),ffse::kKey_Size);

        // generate the new block
        memcpy(block_ptr, (uint8_t *)& block_id,ffse::kID_Size);
        block_ptr += ffse::kID_Size;
        memcpy(block_ptr, mask.data(),ffse::kValue_Size);
        block_ptr += ffse::kValue_Size;
    }

    transfer_update_ptr[thread_num] = block_ptr - transfer_update_buffer[thread_num];
    // write back to the token map
    if(found){
        token_map[thread_num]->at(wid) = pre_bucket;
    }else{
        token_map[thread_num]->add(wid,pre_bucket);
    }

    // send(socket_num[thread_num], UPDATE);
    // send_to(thread_num, block, ffse::kBlock_Size*nblocks);

    delete r;
}

void client_core::flush_update_send_to(int thread_num){
    send(socket_num[thread_num], UPDATE);
    send_to(thread_num, transfer_update_buffer[thread_num], transfer_update_ptr[thread_num]);
    transfer_update_ptr[thread_num] = 0;
}

void client_core::flush_search_send_to(int thread_num){
    send(socket_num[thread_num], SEARCH);
    send_to(thread_num, transfer_search_buffer[thread_num], transfer_search_ptr[thread_num]);
    transfer_search_ptr[thread_num] = 0;
}


void client_core::search_benchmark(double& cp_time, double& commu_time, int thread_num, bool multi_thread){
    struct timeval cp_t1, cp_t2, commu_t1, commu_t2;
    commu_time = 0;
    cp_time = 0;

    int nblocks = indices_map[thread_num].size();
    cout<<"begin to search\t"<<nblocks<<"\tkeywords\n";
    int bucket = ceil(double(nblocks)/ffse::max_TransferBlocks);
    auto it = indices_map[thread_num].begin();
    uint8_t* block_ptr = transfer_search_buffer[thread_num];

    gettimeofday(&cp_t1, NULL);
    for(int m=0; m<bucket-1; m++){
        block_ptr = transfer_search_buffer[thread_num] + transfer_search_ptr[thread_num];

        
        for(int n=0; n<ffse::max_TransferBlocks; n++){
            search(it->second, block_ptr, thread_num);
            it++;
            block_ptr += ffse::kMap_Size;
        }
        transfer_search_ptr[thread_num] = block_ptr - transfer_search_buffer[thread_num];
        
        
        flush_search_send_to(thread_num);
        
        
    }

    block_ptr = transfer_search_buffer[thread_num] + transfer_search_ptr[thread_num];

    gettimeofday(&commu_t1, NULL);
    for(int n=0; n<nblocks - (bucket-1)*ffse::max_TransferBlocks; n++){
        search(it->second, block_ptr, thread_num);
        it++;
        block_ptr += ffse::kMap_Size;
    }
    gettimeofday(&commu_t2, NULL);
    cp_time += ((double)(commu_t2.tv_sec-commu_t1.tv_sec)*1000+(double)(commu_t2.tv_usec-commu_t1.tv_usec)/1000);

    transfer_search_ptr[thread_num] = block_ptr - transfer_search_buffer[thread_num];
    flush_search_send_to(thread_num);
    gettimeofday(&cp_t2, NULL);

    commu_time = (double)(cp_t2.tv_sec-cp_t1.tv_sec)*1000+(double)(cp_t2.tv_usec-cp_t1.tv_usec)/1000;
    commu_time -= cp_time;

    if(multi_thread){
        pthread_mutex_lock(&mutex_go); 
        thread_Go++;
        pthread_mutex_unlock(&mutex_go);
    }
}

void client_core::multi_thread_search_benchmark(double& cp_time, double& commu_time){
    std::vector<thread_search_info> thread_search_data(nthreads);
    thread_Go = 0;
    pthread_t* t = new pthread_t[nthreads];

    for(int i=0; i<nthreads; i++){
        thread_search_data[i].obj = this;
        thread_search_data[i].t_id = i;
        pthread_create(&t[i], NULL,thread_search_benchmark, (void*) &thread_search_data[i]);
    }

    while(thread_Go < nthreads){
        usleep(10);
    }

    cp_time = 0;
    commu_time = 0;
    for(int i=0; i<nthreads; i++){
        cp_time += thread_search_data[i].cp_time;
        commu_time += thread_search_data[i].commu_time;
    }

    delete t;
}
void* client_core::thread_search_benchmark(void* arg){
    thread_search_info* data = static_cast<thread_search_info*>(arg);
    client_core* obj = data->obj;

    obj->search_benchmark(data->cp_time,data->commu_time,data->t_id,true);

    pthread_exit(NULL);
}

void client_core::database_update(std::string folder_name, int thread_num, bool multi_thread){
    string file_name;
    folder_name = PREP_DATA_PREFIX + folder_name;

    // the three varialbes represent a inverted linked list
    string word;
    int n_files;
    uint32_t fid;
    std::vector<uint32_t> fids;
    uint32_t wid;
    uint8_t* block = new uint8_t[ffse::kBlock_Size];

    file_name = folder_name + "/" + to_string(thread_num) + ".txt";
    ifstream origin_file(file_name.c_str());
    if(!origin_file.good()){
        throw file_error("could not open files " + folder_name);
    }

    stringstream origin_data_buf;
    origin_data_buf << origin_file.rdbuf();

    while(!origin_data_buf.eof()){
        // read a inverted list
        origin_data_buf >> word;
        origin_data_buf >> n_files;

        // read the id of the word
        auto it = indices_map[thread_num].find(word);
        if(it == indices_map[thread_num].end()){
            throw std::runtime_error("please generate the indices map first");
        }
        wid = it->second;

        fids.clear();

        for(int j=0; j<n_files; j++){
            origin_data_buf >> fid;
            fids.push_back(fid);
        }

        for(int i=0; i<scale; i++){
            //add_inverted_list(wid, fids, thread_num);
            adv_update_list(wid, fids, thread_num);
        }
        
    }
    flush_update_send_to(thread_num);
    cout<<endl;

    delete block;

    if(multi_thread){
        pthread_mutex_lock(&mutex_go); 
        thread_Go++;
        pthread_mutex_unlock(&mutex_go);
    }
}

void* client_core::thread_database_update(void* arg){
    kw_indices_info* data = static_cast<kw_indices_info*>(arg);
    client_core* obj = data->obj;

    obj->database_update(data->folder_name,data->t_id,true);

    pthread_exit(NULL);
}

void client_core::multi_thread_database_update(std::string folder_name){
    std::vector<kw_indices_info> kw_indices_data(nthreads);
    thread_Go = 0;
    pthread_t* t = new pthread_t[nthreads];

    for(int i=0; i<nthreads; i++){
        kw_indices_data[i].obj = this;
        kw_indices_data[i].folder_name = folder_name;
        kw_indices_data[i].t_id = i;
        pthread_create(&t[i], NULL,thread_database_update, (void*) &kw_indices_data[i]);
    }

    while(thread_Go < nthreads){
        usleep(10);
    }

    delete t;
}

void client_core::search(const uint32_t& wid, uint8_t* block, int thread_num){
    array<uint8_t, ffse::kMap_Size> pre_bucket{{0x00}};
    bool found = token_map[thread_num]->get(wid,pre_bucket);

    if(!found){
        throw token_map_Error(to_string(wid));
    }

    memcpy(block, pre_bucket.data(), ffse::kMap_Size);
}
const string client_core::master_derivation_key() const
{
    return std::string(keyed_prf->key().begin(), keyed_prf->key().end());
}

void client_core::loadnew_indices_map(std::string folder_name , int thread_num, bool multi_thread){
    if(!is_directory(folder_name)){
        throw file_error("could not find folder " + folder_name);
    }

    string file_name;
    string word;
    int n_files;
    int fid;
    int kw_counter = 0;

    /*
    * Store the keyword number
    */
    string client_indice_file_path = PREP_DATA_PREFIX + "kw_indice"+ to_string(thread_num) +".csv";
    ofstream kw_indexer_stream_;
    kw_indexer_stream_.open(client_indice_file_path,std::ios_base::app |std::ios_base::out);
    if (!kw_indexer_stream_.is_open()) {
        kw_indexer_stream_.close();
        throw file_error("could not open files " + client_indice_file_path);
    }

    /*
    * Store the keyword indices
    */
    string counter_file_path = PREP_DATA_PREFIX + "kw_counter"+ to_string(thread_num) +".stat";
    ofstream counter_file_stream_;
    counter_file_stream_.open(counter_file_path,std::ios_base::out);
    if (!counter_file_stream_.is_open()) {
        counter_file_stream_.close();
        throw file_error("could not open files " + counter_file_path);
    }



    file_name = folder_name + "/" + to_string(thread_num) + ".txt";
    ifstream origin_file(file_name.c_str());
    if(!origin_file.good()){
        throw file_error("could not open files " + folder_name);
    }

    stringstream origin_data_buf;
    origin_data_buf << origin_file.rdbuf();

    while(!origin_data_buf.eof()){
        kw_counter ++;
        origin_data_buf >> word;
        origin_data_buf >> n_files;
        //indices_map.insert(std::make_pair(word, n_files));
        for(int j=0; j<n_files; j++){
            origin_data_buf >> fid;
        }

        kw_indexer_stream_ << word << "\t" << kw_counter << "\n";
    }

    cout<<file_name<< " has keywords, reolaod complete\n";
    cout << "total keywords\t" << kw_counter <<endl;
    origin_file.close();       

    counter_file_stream_ << kw_counter << "\n";
    counter_file_stream_.close();
    kw_indexer_stream_.close();

    if(multi_thread){
        pthread_mutex_lock(&mutex_go); 
        thread_Go++;
        pthread_mutex_unlock(&mutex_go);
    }
}

void client_core::multi_thread_loadnew_indices_map(std::string folder_name){
    std::vector<kw_indices_info> kw_indices_data(nthreads);
    thread_Go = 0;
    pthread_t* t = new pthread_t[nthreads];

    for(int i=0; i<nthreads; i++){
        kw_indices_data[i].obj = this;
        kw_indices_data[i].folder_name = folder_name;
        kw_indices_data[i].t_id = i;
        pthread_create(&t[i], NULL,thread_loadnew_indices, (void*) &kw_indices_data[i]);
    }

    while(thread_Go < nthreads){
        usleep(10);
    }

    delete t;
}

void* client_core::thread_loadnew_indices(void* arg){
    kw_indices_info* data = static_cast<kw_indices_info*>(arg);
    client_core* obj = data->obj;

    obj->loadnew_indices_map(data->folder_name,data->t_id,true);

    pthread_exit(NULL);
}
void client_core::start(){

    struct timeval t1;
    struct timeval t2;
    double cpu_time_used;
    size_t bytes_comm;

    double cp_time = 0, commu_time = 0;


    if(load_kw_indices){
        multi_thread_loadnew_indices_map("Enron/Enron_invertedlinkedList");
    }

    if(test_update){
        printf("begin update \n");
        gettimeofday(&t1, NULL);
        multi_thread_database_update("Enron/Enron_invertedlinkedList"); 
        gettimeofday(&t2, NULL);

        cpu_time_used = (double)(t2.tv_sec-t1.tv_sec)*1000+(double)(t2.tv_usec-t1.tv_usec)/1000;
        printf("ffse update Enron database need  %f (ms) \n",cpu_time_used);

        bytes_comm = report_size();
        printf("ffse update Enron database send  %ld (KB) \n",bytes_comm/1000);
    }

    if(test_search){
        printf("begin search \n");
        //search_benchmark(cp_time[0],commu_time[0],0);
        multi_thread_search_benchmark(cp_time,commu_time);
        printf("ffse search:\tClient Computation need  %f (ms) \t Communication need  %f (ms) \n",cp_time, commu_time);

        bytes_comm = report_size();
        printf("ffse search Enron database send  %ld (KB) \n",bytes_comm/1000);
    }


    for(int i=0; i<nthreads; i++){
        send(socket_num[i], TERMINATE);
    }
}

void client_core::receive_from(int thread_num, uint8_t* data, size_t& data_len) const{
    receive(socket_num[thread_num], data_len,LENGTH_SIZE);
    receive(socket_num[thread_num], data, data_len);
}

void client_core::send_to(int thread_num, uint8_t* data, size_t data_len){
    send(socket_num[thread_num], data_len, LENGTH_SIZE);
    send(socket_num[thread_num], data, data_len);
    sent_bytes[thread_num] += data_len;
}

size_t client_core::report_size(){
    size_t result = 0;
    for(int i=0; i<nthreads; i++){
        result += sent_bytes[i];
        sent_bytes[i] = 0;
    }
    return result;
}