// (C) 2019 University of NKU. Free for used
// Author: stoneboat@mail.nankai.edu.cn

/*
* client_core.h
*
*/

#include <string>
#include <array>
#include <vector>
#include <sse/crypto/prf.hpp>
#include <ssdmap/bucket_map.hpp>
#include <atomic>
#include <fstream>
#include <map>
#include <pthread.h>
#include "Tools/utils.h"


/*
*	@class client_core
*	@functionality basic functionality of client
*	@Params of the Class client_core
*		@PREP_DATA_PREFIX   The position where the local data of client is stored 
*		@keyed_prf          Local master symmetric key
*		@token_map			Local map file which store the pair(word_id, token)
*		@indices_map		Local map file which store the pair(word, word_id)
*		@transfer_buffer	the buffer waiting for transfer block
*/


class client_core{
private:
	sse::crypto::Prf<ffse::kKey_Size>* keyed_prf;

	std::vector<std::map<std::string, uint32_t>> indices_map;
	std::vector<ssdmap::bucket_map<uint32_t, std::array<uint8_t, ffse::kMap_Size>>*> token_map;

	int nthreads;

	/*
	* benchmark parameter
	*/

	bool test_update;
	bool test_search;
	bool load_kw_indices;
	int scale;
	std::vector<size_t> id_counter;
	std::vector<size_t> sent_bytes;

	// for multi-thread
	pthread_mutex_t mutex_go;
  	int thread_Go;
public:
	client_core(int argc, const char** argv);
	~client_core();

	void start();

	const std::string master_derivation_key() const;

	/*
	*	op:	add for 1; delete for 0
	*	update a key-value pair
	*/
	void update(const uint32_t& word, uint32_t fid, const bool& op, int thread_num);

	// this functionality update a whole inverted list of a words, it is not safe since will leak the size and link info of the words
	// it's just used for benchmark to accelerate the rate of established encrypted database since we don't need to search the client's local map
	// indices n times
	void add_inverted_list(const uint32_t& word, std::vector<uint32_t>& fids, int thread_num);
	void adv_update_list(const uint32_t& word, std::vector<uint32_t>& fids, int thread_num);

	void search(const uint32_t& wid, uint8_t* block, int thread_num=0);



	/*
	* This function update the whole database in several batch from the preprocessed nvertedlinkedList
	*/
	void database_update(std::string folder_name = "Enron/Enron_invertedlinkedList", int thread_num = 0, bool multi_thread = false);
	void multi_thread_database_update(std::string folder_name = "Enron/Enron_invertedlinkedList");
	static void* thread_database_update(void* arg);

	/*
	* This function test the search performance searching all the keywords in the indices_map
	*/
	void search_benchmark(double& cp_time, double& commu_time, int thread_num = 0, bool multi_thread = false);
	void multi_thread_search_benchmark(double& cp_time, double& commu_time);
	static void* thread_search_benchmark(void* arg);



	/*
	* This function read the original inverted linked files and store back in indices_map
	*/
	void loadnew_indices_map(std::string folder_name = "Enron/Enron_invertedlinkedList", int thread_num = 0, bool multi_thread = false);

	void multi_thread_loadnew_indices_map(std::string folder_name = "Enron/Enron_invertedlinkedList");
	static void* thread_loadnew_indices(void* arg);


	/*
	* basic communication functionality
	*/
	// receive from client thread_num
	void receive_from(int thread_num, uint8_t* data, size_t& data_len) const;

	// Send to client thread_num
	void send_to(int thread_num, uint8_t* data, size_t data_len);

	// To reduce the communication rounds, batch flush
	void flush_update_send_to(int thread_num);
	void flush_search_send_to(int thread_num);

	size_t report_size();


private:
	std::ofstream keyword_indexer_stream_;
	std::string PREP_DATA_PREFIX;

	/*
	*	Network part
	*/
	int pnbase;
	std::string hostname;
	std::vector<int> socket_num;

	std::vector<uint8_t*> transfer_update_buffer;
	std::vector<size_t>	  transfer_update_ptr;
	std::vector<uint8_t*> transfer_search_buffer;
	std::vector<size_t>	  transfer_search_ptr;
};