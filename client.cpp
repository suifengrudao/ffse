// (C) 2019 University of NKU. Free for used
// Author: stoneboat@mail.nankai.edu.cn

/*
 * client.cpp
 *
 */
#include<iostream>
#include"ffse_Client/client_core.h"

using namespace std;

int main(int argc, char** argv)
{
    client_core(argc,(const char**) argv).start();
}

