import re
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from nltk.stem.porter import PorterStemmer  
from threading import Thread
from math import ceil
import sys

stopworddic = set(stopwords.words('english'))
tokenizer = RegexpTokenizer(r'\w+')
porter_stemmer = PorterStemmer()  

files_number = 517401

def extract_linkedList(thread_id, start_file, file_num):
	print(str(thread_id)+" starts with file  "+str(start_file)+"  to  "+ str(start_file+file_num))
	count = start_file
	file_name = 'Enron_linkedList/'+str(thread_id)+'.txt'
	processed_file = open(file_name,'a') 

	for i in range(file_num):
		file_name = 'Enron_datasets/'+str(count)+'.txt'
		count = count + 1
		origin_file = open(file_name) 
		origin_data = ""
		try:
			origin_data = origin_file.read()
			wordsList = tokenizer.tokenize(origin_data)
			wordsList = [i for i in wordsList if i not in stopworddic]
			wordsList = [porter_stemmer.stem(i) for i in wordsList ]
			wordsList = list(set(wordsList))
			processed_file.write(str(count)) # write the file id
			processed_file.write('\t')
			processed_file.write(str(len(wordsList))) # write the number of the words in file
			for word in wordsList:
				processed_file.write('\t')
				processed_file.write(word)
			processed_file.write('\n')

		except Exception as e:
			print("could not properly open file:  "+ file_name)
		finally:
			origin_file.close()

	processed_file.close()

	print(str(thread_id)+" finishes and exit")

if __name__=="__main__":
	# Thread parameter
	if(len(sys.argv) == 1):
		nthreads = 1
	else:
		nthreads = int(sys.argv[1])
	threads = []
	# bucket parameter
	bucket = ceil(files_number/nthreads)
	start = 2

	for i in range(nthreads):
		t = Thread(target=extract_linkedList, args=(i,start,bucket))
		start = start + bucket
		if start+bucket > files_number:
			bucket = files_number-start
		t.start()
		threads.append(t)

	for t in threads:
		t.join()