from math import ceil
import sys

def extract_invertedLinkedList(nthreads):
	invertedDic = {}

	#	read the linked list and reorganize it as inverted linked list
	for thread_id in range(nthreads):
		file_name = 'Enron_linkedList/'+str(thread_id)+'.txt'
		print(" starts with file  "+file_name)
		origin_file = open(file_name) 
		try:
			origin_data = origin_file.read()
			fileLists = origin_data.split('\n')
			for i in range(len(fileLists)):
				wordsList = fileLists[i].split('\t')
				file_id = int(wordsList[0])
				n_keywords = int(wordsList[1])
				for j in range(2,len(wordsList)):
					if wordsList[j] not in invertedDic:
						invertedDic[wordsList[j]] = []
					invertedDic[wordsList[j]].append(file_id)
		except Exception as e:
			print("Error could not properly open file:  "+ file_name)
		finally:
			origin_file.close()


	bucket = ceil(len(invertedDic)/nthreads)
	thread_id = 0
	counter = 0
	file_name = 'Enron_invertedlinkedList/'+str(thread_id)+'.txt'
	processed_file = open(file_name,'w') 
	file_name = 'Enron_invertedlinkedList/summary.txt'
	summary_file = open(file_name,'w') 

	summary = {}

	#	store back the inverted linked list and get some statistic results
	for word in invertedDic:
		tmp_set = invertedDic[word]
		len_list = len(tmp_set)

		#	get some statistic results
		if len_list not in summary:
			summary[len_list] = 0
		summary[len_list] += 1

		#	store back the inverted linked list
		processed_file.write(word) # write the word
		processed_file.write('\t')
		processed_file.write(str(len_list)) # write the length of inverted list
		for fid in tmp_set:
			processed_file.write('\t')
			processed_file.write(str(fid))
		processed_file.write('\n')

		counter += 1
		if counter >= bucket:
			counter = 0
			thread_id += 1
			processed_file.close()
			file_name = 'Enron_invertedlinkedList/'+str(thread_id)+'.txt'
			processed_file = open(file_name,'a') 

	processed_file.close()
	summary_file.write(str(len(invertedDic)))
	summary_file.write('\n')
	for len_report in summary:
		summary_file.write(str(len_report))
		summary_file.write('\t')
		summary_file.write(str(summary[len_report]))
		summary_file.write('\n')

	

	print(" finishes and exit")

if __name__=="__main__":
	# Thread parameter
	if(len(sys.argv) == 1):
		nthreads = 1
	else:
		nthreads = int(sys.argv[1])
	threads = []

	extract_invertedLinkedList(nthreads)

