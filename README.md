# Code to support the paper FFSE: Forward Secure Searchable Encryption with Keyed-block Chains
## Pre-requisites
+   ffse need a compiler supporting C++11. It has been successfully built and tested on Ubuntu 16 LTS using both gcc 5.4 and gcc 7.2
+   `sudo apt-get install build-essential autoconf libtool yasm openssl scons`

### Installing RocksDB
+   `git clone -b 4.9.fb https://github.com/facebook/rocksdb.git`
+   `[sudo] make install`

### Installing ssdmap and crypto
+   `git clone https://github.com/rbost/ssdmap.git`
+   `cd ssdmap && scons`
+   move the built header files and lib files in /usr/local/include and /usr/local/lib
+   `git clone https://gitlab.com/sse/crypto.git`
+   `cd crypto && scons`
+   move the built header files and lib files in /usr/local/include and /usr/local/lib



## Preprocess the EnronData
+   Download Enron data `wget https://www.cs.cmu.edu/~./enron/enron_mail_20150507.tar.gz` or `axel -n {nthreads}  https://www.cs.cmu.edu/~./enron/enron_mail_20150507.tar.gz`
+   unzip it with `tar -xvf {eron_file_name}` , use `ls -lR|grep "^-"|wc -l` can show the total files in the datasets.
+   `mkdir Enron_datasets` and move the script 'renameEnronDir.sh' to the same dir where 'Enron_datasets' folder and unzip Enron data 'maildir' resides.
+   run the scripts `bash renameEnronDir.sh maildir` which will copy the 'maildir' into the 'Enron_datasets' folder with renamed name of each file. This will help the code to easily transfer the mail into key-valu pair in parallel mode.
+   move 'extractEnronLinked.py' to the same dir where 'Enron_datasets' folder resides and run `mkdir Enron_linkedList && python extractEnronLinkedList.py {nthreads}`, then the linked list will be stored in folder 'Enron_linkedList'.
+   move 'extractEnronInvertedList.py' to the same dir where 'Enron_linkedList' folder resides and run `mkdir Enron_invertedlinkedList && python extractEnronLinkedList.py {nthreads}`, then the inverted linked list will be stored in folder 'Enron_invertedlinkedList'.

## Run the benchmark
### Load the Keywords Indices
+   For default, `mkdir Enron` under the folder of ffse. Move the generated folder 'Enron_linkedList' and 'Enron_invertedlinkedList' under it . `mkdir Enron` under the folder `Client_Storage` and move the generated folder 'Enron_linkedList' and 'Enron_invertedlinkedList' under it.
+   `make online -j {nthreads}`
+   open a new cmd and type `\server.x -nt {nthreads}`
+   open a new cmd and type `\client.x -nt {nthreads} -h {localhost} -l 1`
+   Note, if you encounter any wrong, run `make clean-local` and try again

### Update the database
+   open a new cmd and type `\server.x -nt {nthreads}`
+   open a new cmd and type `\client.x -nt {nthreads} -h {localhost} -u 1`
+   if you want to larger the tested database, run `\client.x -nt {nthreads} -h {localhost} -x {number} -l 1`, the parameter {number} will scale the enron_database with {number} times
+   If you want to clean the database, run `make clean-local`

### Search the database
+   open a new cmd and type `\server.x -nt {nthreads}`
+   open a new cmd and type `\client.x -nt {nthreads} -h {localhost} -s 1`
