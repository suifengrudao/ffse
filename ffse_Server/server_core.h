// (C) 2019 University of NKU. Free for used
// Author: stoneboat@mail.nankai.edu.cn

/*
* server_core.h
*
*/
#include "Tools/utils.h"
#include "Networking/ServerSocket.h"
#include "Tools/time-func.h"
#include "rocksdb_wrapper.h"
#include <string>
#include <vector>
#include <pthread.h>



class server_core{
private:
	int nthreads;
	int pnbase;

	// for multi-thread
	pthread_mutex_t mutex_go;
  	int thread_Go;
public:
	void start();
	void start_db_handle(int thread_num, bool multi_thread = false);
	static void* thread_start_db_handle(void* arg);

	server_core(int argc, const char** argv);
	~server_core();

	mutable std::vector<ServerSocket*> server;
	std::vector<int> socket_num;

	void search(const uint8_t* token, std::vector<uint32_t>& file_lists, double& db_cost ,int thread_num = 0);

	/*
	* basic communication functionality
	*/
	// receive an octetstream from client thread_num
	void receive_from(int thread_num, uint8_t* data, size_t& data_len) const;

	// Send an octetStream to client thread_num
	void send_to(int thread_num, uint8_t* data, size_t data_len) const;

private:
	std::string PREP_DATA_PREFIX;
	std::vector<RockDBWrapper> db;
	// remain the communication load
	mutable size_t sent;
};