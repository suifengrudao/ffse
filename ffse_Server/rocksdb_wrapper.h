// (C) 2019 University of NKU. Free for used
// Author: stoneboat@mail.nankai.edu.cn

/*
* rocksdb_wrapper.cpp
*
*/

#pragma once

#include "Tools/utils.h"
#include "Exceptions/Exceptions.h"

#include <rocksdb/db.h>
#include <rocksdb/table.h>
#include <rocksdb/memtablerep.h>
#include <rocksdb/options.h>

#include <iostream>
#include <string>


        
class RockDBWrapper {
public:
    inline RockDBWrapper();
    inline void start(const std::string &path);
    inline ~RockDBWrapper();
    
    inline bool get(uint8_t* key, const size_t& key_len, uint8_t* data, const size_t& data_len);
    
    inline bool put(uint8_t* block, size_t id_size = ffse::kID_Size, size_t value_size = ffse::kValue_Size , int nblocks = 1);
    inline bool batch_put(uint8_t* block, size_t id_size = ffse::kID_Size, size_t value_size = ffse::kValue_Size , int nblocks = 1);
    
private:
    rocksdb::DB* db_;
    bool db_start;
    rocksdb::Options options;
    
};

RockDBWrapper::RockDBWrapper() : db_(NULL) {
    options.create_if_missing = true;

    db_start = false;
}

void RockDBWrapper::start(const std::string &path)
{
    rocksdb::Status status = rocksdb::DB::Open(options, path, &db_);
    
    if (!status.ok()) {
        throw db_Error(" Unable to open the database:  " + status.ToString());
        db_ = NULL;
    }

    db_start = true;
}

RockDBWrapper::~RockDBWrapper()
{
    if (db_) {
        delete db_;
    }
}


bool RockDBWrapper::get(uint8_t* key, const size_t& key_len, uint8_t* data, const size_t& data_len)
{
    if( !db_start ){
        throw db_Error(" run init function to start the database first ");
    }
    rocksdb::Slice k_s((const char*) key, key_len);
    std::string value;
    
    rocksdb::Status s = db_->Get(rocksdb::ReadOptions(false,true), k_s, &value);
    
    if(s.ok()){
        ::memcpy(data, value.data(), data_len);
    }else{
        throw db_Error(" could not get value from database ");
    }
    
    return s.ok();
}

bool RockDBWrapper::put(uint8_t* block, size_t id_size, size_t value_size, int nblocks)
{
    if( !db_start ){
        throw db_Error(" run init function to start the database first ");
    }
    uint8_t* block_ptr = block;
    rocksdb::Status s;
    bool ret = true;
    for(int i=0; i<nblocks; i++){
        rocksdb::Slice k_s((const char*) block_ptr,id_size);
        block_ptr += id_size;
        rocksdb::Slice k_v((const char*) block_ptr,value_size);
        block_ptr += value_size;
        s = db_->Put(rocksdb::WriteOptions(), k_s, k_v);
        if (!s.ok()){
            throw db_Error(" Insert failure ");
        }
        ret &= s.ok();
    }
    return ret;
}
     
bool RockDBWrapper::batch_put(uint8_t* block, size_t id_size, size_t value_size, int nblocks){
    if( !db_start ){
        throw db_Error(" run init function to start the database first ");
    }
    rocksdb::WriteBatch batch;
    uint8_t* block_ptr = block;
    rocksdb::Status s;
    for(int i=0; i<nblocks; i++){
        rocksdb::Slice k_s((const char*) block_ptr,id_size);
        block_ptr += id_size;
        rocksdb::Slice k_v((const char*) block_ptr,value_size);
        block_ptr += value_size;
        batch.Put(k_s, k_v);
        
    }
    s = db_->Write(rocksdb::WriteOptions(), &batch);
    if (!s.ok()){
        throw db_Error(" Insert failure ");
    }
    return true;
}  
