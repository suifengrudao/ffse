// (C) 2019 University of NKU. Free for used
// Author: stoneboat@mail.nankai.edu.cn

/*
* server_core.cpp
*
*/

#include "server_core.h"
#include "Tools/ezOptionParser.h"
#include "Exceptions/Exceptions.h"
#include "Networking/sockets.h"
#include "Networking/data.h"
#include "Tools/utils.h"

#include <sse/crypto/prf.hpp>

#include <iostream>

using namespace std;

typedef struct database_handle_struct{
   server_core* obj;
   int t_id;
} db_handle_info;

server_core::server_core(int argc, const char** argv){
	ez::ezOptionParser opt;

    opt.syntax = "./server.x [OPTIONS]\n";
    opt.add(
        "", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "Directory containing the client data (default: Server_Storage)", // Help description.
        "-d", // Flag token.
        "--dir" // Flag token.
    );
    opt.add(
        "1", // Default.
        0, // Required?
        1, // Number of args expected.
        0, // Delimiter if expecting multiple args.
        "number of the threads(default 1)", // Help description.
        "-nt", // Flag token.
        "--number_threads" // Flag token.
    );
    opt.add(
      "5000", // Default.
      0, // Required?
      1, // Number of args expected.
      0, // Delimiter if expecting multiple args.
      "Port number base to attempt to start connections from (default: 5000)", // Help description.
      "-pn", // Flag token.
      "--portnumbase" // Flag token.
    );

    opt.parse(argc, argv);

    opt.get("--number_threads")->getInt(nthreads);
    opt.get("--portnumbase")->getInt(pnbase);

    
    if (opt.isSet("--dir"))
    {
        opt.get("--dir")->getString(PREP_DATA_PREFIX);
        PREP_DATA_PREFIX += "/";
    }
    else{
        PREP_DATA_PREFIX = "Server_Storage/";
    }

    /*
    *	create the local data folder if it is not exists
    */
    if(!is_directory(PREP_DATA_PREFIX)){
    	if(!create_directory(PREP_DATA_PREFIX,S_IRWXU)){
    		throw file_error("could not mkdir " + PREP_DATA_PREFIX);
    	}
    }


    db.resize(nthreads);
    for(int i=0; i<nthreads; i++){
        std::string database_path = PREP_DATA_PREFIX + "database"+ to_string(i) + ".dat";
        cout<<database_path<<endl;
        db[i].start(database_path);
    }
    
    


    /*
    *	initialization networking part
    */
    server.resize(nthreads);
    int port_num = pnbase;
    for(int i=0; i<nthreads; i++){
    	server[i] = new ServerSocket(port_num);
    	server[i]->init();
    	port_num += ffse::port_increase;
    }
    socket_num.resize(nthreads);
    
    for(int i=0; i<nthreads; i++){
        cout << "Waiting for client thread " << i << endl;
        socket_num[i] = server[i]->get_connection_socket(i);
        cout << "Connected to client thread " << i << endl;

        send(socket_num[i], GO);    
    }

    // for multi-thread
    mutex_go = PTHREAD_MUTEX_INITIALIZER;
    
}
void server_core::start_db_handle(int thread_num, bool multi_thread){
    uint8_t* block = new uint8_t[ffse::kBlock_Size*ffse::max_TransferBlocks];
    uint8_t* token = new uint8_t[ffse::kMap_Size*ffse::max_TransferBlocks];
    size_t data_len;
    //int ptr = 0;
    int nblocks = 0;
    int ntokens = 0;

    int total_blocks = 0;
    int total_tokens = 0;

    int inst = -1;
    bool loop = true;

    //time counter
    struct timeval t1;
    struct timeval t2;
    double search_commu_time = 0, search_cpu_time = 0, search_db_time = 0;
    double update_commu_time = 0, update_db_time = 0;

    using ffse::CT;
    const size_t CT_Block_Size = ffse::kBlock_Size + (CT-1)*ffse::Ind_Size;
    const size_t CT_Value_Size = ffse::kValue_Size + (CT-1)*ffse::Ind_Size;

    vector<uint32_t> fid_list;
    while (loop){
        receive(socket_num[thread_num], inst); 
        switch(inst){
            case TERMINATE:
                loop = false;
                cout<<"\nreceive TERMINATE instruction and exits\n";
                break;
            case UPDATE:
                //cout<<"receive UPDATE instruction\n";
                gettimeofday(&t1, NULL);
                receive_from(thread_num, block, data_len);
                gettimeofday(&t2, NULL);
                update_commu_time += (double)(t2.tv_sec-t1.tv_sec)*1000+(double)(t2.tv_usec-t1.tv_usec)/1000;

                nblocks = data_len/CT_Block_Size;
                total_blocks += nblocks;

                gettimeofday(&t1, NULL);
                //db.put(block, ffse::kID_Size, ffse::kValue_Size , nblocks);
                db[thread_num].batch_put(block, ffse::kID_Size, CT_Value_Size , nblocks);
                gettimeofday(&t2, NULL);
                update_db_time += (double)(t2.tv_sec-t1.tv_sec)*1000+(double)(t2.tv_usec-t1.tv_usec)/1000;
                //printf("ffse update %d blocks need  %f (ms) \n",nblocks, update_db_time);
                printf("..");

                break;
            case SEARCH:
                //cout<<"receive SEARCH instruction\n";
                gettimeofday(&t1, NULL);
                receive_from(thread_num, token, data_len);
                gettimeofday(&t2, NULL);
                search_commu_time += (double)(t2.tv_sec-t1.tv_sec)*1000+(double)(t2.tv_usec-t1.tv_usec)/1000;

                ntokens = data_len/ffse::kMap_Size;
                total_tokens += ntokens;
                gettimeofday(&t1, NULL);
                for(int i=0; i<ntokens; i++){
                    search(token+ffse::kMap_Size*i, fid_list, search_db_time, thread_num);
                }
                gettimeofday(&t2, NULL);
                search_cpu_time += (double)(t2.tv_sec-t1.tv_sec)*1000+(double)(t2.tv_usec-t1.tv_usec)/1000;
                search_cpu_time -= search_db_time;

                break;
            default:
                break;
        }
    }
    printf("ffse update %d blocks :\tCommunication need  %f (ms)\t database need  %f (ms) \n",total_blocks, update_commu_time, update_db_time);
    printf("ffse search %d keywords :\tServer Computation need  %f (ms) \t Communication need  %f (ms)\t database need  %f (ms) \n",total_tokens, search_cpu_time, search_commu_time, search_db_time);

    delete block;
    delete token;

    if(multi_thread){
        pthread_mutex_lock(&mutex_go); 
        thread_Go++;
        pthread_mutex_unlock(&mutex_go);
    }
}
void server_core::start(){
    //start_db_handle(0);

    std::vector<db_handle_info> db_handle_data(nthreads);
    thread_Go = 0;
    pthread_t* t = new pthread_t[nthreads];

    for(int i=0; i<nthreads; i++){
        db_handle_data[i].obj = this;
        db_handle_data[i].t_id = i;
        pthread_create(&t[i], NULL,thread_start_db_handle, (void*) &db_handle_data[i]);
    }

    while(thread_Go < nthreads){
        usleep(10);
    }

    delete t;
}

void* server_core::thread_start_db_handle(void* arg){
    db_handle_info* data = static_cast<db_handle_info*>(arg);
    server_core* obj = data->obj;

    obj->start_db_handle(data->t_id,true);

    pthread_exit(NULL);
}
bool equal(uint8_t* a, uint8_t* b, const size_t& len){
    bool ret = true;
    for(size_t i=0; i<len; i++){
        ret &= (a[i] == b[i]);
    }
    return ret;
}

void server_core::search(const uint8_t* _token, std::vector<uint32_t>& file_lists, double& db_cost ,int thread_num ){
    using ffse::CT;
    const size_t CT_Value_Size = ffse::kValue_Size + (CT-1)*ffse::Ind_Size;

    file_lists.clear();
    uint8_t token[ffse::kMap_Size];
    uint8_t block[CT_Value_Size];
    memcpy(token, _token, ffse::kMap_Size);
    array<uint8_t, ffse::kMap_Size> empty_bucket{{0x00}};
    uint32_t fid = 0;
    uint8_t* p = 0;
    //bool op;
    int offset; 

    struct timeval t1;
    struct timeval t2;
    db_cost = 0;

    do{
        gettimeofday(&t1, NULL);
        db[thread_num].get(token, ffse::kID_Size, block, CT_Value_Size);
        gettimeofday(&t2, NULL);
        db_cost += (double)(t2.tv_sec-t1.tv_sec)*1000+(double)(t2.tv_usec-t1.tv_usec)/1000;

        sse::crypto::Prf<CT_Value_Size> tmp_keyed_prf((void*)(token+ffse::kID_Size),ffse::kKey_Size);
        array<uint8_t, CT_Value_Size> mask = tmp_keyed_prf.prf(token, ffse::kID_Size);
        for(size_t i= 0; i< CT_Value_Size; i++){
            block[i] ^= mask[i];
        }

        memcpy(token, block, ffse::kMap_Size);
        offset = ffse::kMap_Size;

        for(int k=0; k<CT; k++){
            p = (uint8_t *)& fid;
            for(size_t i=0; i<ffse::Ind_Size; i++){
                p[i] = block[offset++];
            }
            file_lists.push_back(fid);
        }
        
        //op = fid & 1;
        //fid = (fid >> 1);    
    }while(! equal(token,empty_bucket.data(),ffse::kMap_Size));

    //cout<<"search complete\n";
    
}

server_core::~server_core(){
	for(int i=0; i<nthreads; i++){
        delete server[i];
        close(socket_num[i]);
    }
}


void server_core::receive_from(int thread_num, uint8_t* data, size_t& data_len) const{
    receive(socket_num[thread_num], data_len,LENGTH_SIZE);
    receive(socket_num[thread_num], data, data_len);
}

void server_core::send_to(int thread_num, uint8_t* data, size_t data_len) const{
    send(socket_num[thread_num], data_len, LENGTH_SIZE);
    send(socket_num[thread_num], data, data_len);
}
